

![](./images/JSSDK.png)


## 介绍


JSSDK 是用于屏蔽 JavaScript 与 Android/iOS 平台间通信的差异。使移动网页开发人员无需关心两端在 Bridge 通信上的差异。为移动网页开发提供更多的原生能力，解决移动网页能力不足的问题。


项目开发人员可以根据具体的业务需求自定义原生能力，通过 JSSDK 可以非常便捷的完成通信过程，为移动网页开发提供一套便捷访问原生能力的 API，同时也为移动开发人员提供一套便捷访问 JavaScript 方法的能力。


## 特征

**JSSDK 框架可以帮助快速构建如下两种交互场景：**

- **JavaScript 调用原生自定义事件/方法，并直接 return/callback 回 JavaScript** 的调用者
- **原生调用 JavaScript 方法，并接收来自 JavaScript 方法的 callback**

## 包含内容

JSSDK 主要包含三部分内容：**当前为 Android 部分**

- [JavaScript 部分，移动网页与原生交互的入口](https://gitee.com/godliness-keep/jssdk-javascript/blob/master/README.md)
- **Android 部分，Android 平台通信桥梁**
- iOS 部分，iOS 平台通信桥梁

## 接入

### 一、添加依赖


##### 1. Add the JitPack repository to your build file

Add it in your root build.gradle at the end of repositories:

    allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}


##### 2. Add the dependency

    dependencies {
            // 依赖jssdk组件
	        implementation 'com.gitee.godliness-keep.jssdk:jssdk:选择最新版本号'
	        <!--// 如果使用腾讯的 TBS WebView 则依赖 jssdk-x5 组件，二选一-->
	        <!--implementation 'com.gitee.godliness-keep.jssdk:jssdk-x5:选择最新版本号'-->
	        
	        // 无论使用 JSSDK 或 JSSDK-X5 都需要依赖该处理器
            annotationProcessor 'com.gitee.godliness-keep.jssdk:jssdk-apt:选择最新版本号'
	}


> 注意：jssdk-x5 是基于腾讯 TBS-WebView，根据项目使用的 WebView 情况选择合适的 jssdk

---


### 二、配置

##### 1. 扩展 BaseBridge

```
public final class TestBridge extends BaseBridge<X5WebViewActivity> {

   /**
    * Called after {@link BaseBridge#bindTarget(Object, WebView)}
    */
    @Override
    protected void onBind() {
        // 在 bindTarget 之后回调
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 在 X5WebViewActivity onDestroy 后回调
    }
}
```


##### 2. 绑定桥梁

```
final TestBridge x5Bridge = new TestBridge();
// T 可以是 Activity(FragmentActivity 的子类) 或 Fragment(v4)
x5Bridge.bindTarget(this, mWebView);
```

---


## 如何使用

使用分为两部分：

- 第一部分：JavaScript 调用到 Native
- 第二部分：Native 调用到 JavaScript


### 第一部分：JavaScript 调用到 Native 

JavaScript 调用到 Native，从功能定义上分为如下两种方式：

1. JavaScript 调用 Native 事件
2. JavaScript 调用 Native 方法

两种方式分别有各自的适用场景，下面分别介绍两种方式的使用区别和注意事项。

#### 一、JavaScript 调用到 Native 事件


首先来看，JavaScript 调用 Native 事件； 原生通过**自定义事件**方式，接收由 JavaScript 的调用。**事件类型**总共分为如下**四种场景**：

- **无参无返回值**
- **有参无返回值**
- **无参有返回值**
- **有参有返回值**


**自定义事件类型，内部会自动管理当前事件的生命周期；例如当前 Activity/Fragment 生命周期结束后，在该 Activity/Fragment 中定义的事件将自动销毁，不必担心引发内存泄漏**

---

##### 1. 自定义事件声明


**（1）无参无返回值**


    /**
     * 声明无参数，无返回值类型的事件-打开设置
     */
    private final IReceiver mSettingReceiver = new IReceiver() {
        @EventName("toSetting")
        @Override
        protected void onEvent() {
            Log.i(TAG, "toSetting");
        }
    };

**注意：对于无 return 场景的事件，其内部提供了 callback 机制，同样可以实现返回到 JavaScript 调用者，示例如下：**

    private final IReceiver mSettingReceiver = new IReceiver() {
        @EventName("toSetting")
        @Override
        protected void onEvent() {
            Log.i(TAG, "toSetting");

            /**
             * 注意无返回值类型，可以使用 {@link com.longrise.android.jssdk.receiver.IReceiver#callback(Object)} 机制，返回到 JavaScript 调用者
             * 具体取决于业务场景
             * */
            final Bean bean = new Bean();
            bean.desc = "收到";
            bean.state = 1;
            // 使用 callback 机制
            callback(bean);
        }
    };

**（2）无参有返回值**


    /**
     * 声明无参数，有返回值类型的事件-获取用户信息
     */
    private final IReturnReceiver<UserInfo> mGetUserInfoReceiver = new IReturnReceiver<UserInfo>() {

        @EventName("getUserInfo")
        @Override
        protected UserInfo onEvent() {
            final UserInfo info = new UserInfo();
            info.name = "godliness";
            info.age = 100;
            info.sex = "boy";
            return info;
        }
    };


**（3）有参无返回值**


    /**
     * 声明有参数，无返回值类型的事件-拨打电话
     */
    private final IParamsReceiver<String> mCallReceiver = new IParamsReceiver<String>() {

        @EventName("call")
        @Override
        protected void onEvent(String params) {
            Log.i(TAG, "mobile: " + params);
        }
    };

**注意：对于无 return 场景的事件，其内部提供了 callback 机制，同样可以实现返回到 JavaScript 调用者，示例如下：**


    private final IParamsReceiver<String> mCallReceiver = new IParamsReceiver<String>() {

        @EventName("call")
        @Override
        protected void onEvent(String params) {
            Log.i(TAG, "mobile: " + params);

            /**
             * 注意无返回值类型，可以使用 {@link com.longrise.android.jssdk.receiver.IReceiver#callback(Object)} 机制，返回到 JavaScript 调用者
             * 具体取决于业务场景
             * */
            final Bean bean = new Bean();
            bean.desc = "收到";
            bean.state = 1;
            // 使用 callback 机制
            callback(bean);
        }
    };


**（4）有参有返回值**

    /**
     * 声明有参数，有返回值类型的事件-根据条件返回用户信息
     */
    private final IParamsReturnReceiver<Condition, UserInfo> mConditionUserInfoReceiver = new IParamsReturnReceiver<Condition, UserInfo>() {

        @EventName("conUserInfo")
        @Override
        protected UserInfo onEvent(Condition params) {
            Log.i(TAG, "需要获取用户名为: " + params.name + " 的个人资料");

            // do next

            final UserInfo info = new UserInfo();
            info.sex = "boy";
            info.age = 100;
            info.address = "china";
            info.company = "free";
            return info;
        }
    };


##### 2. 注册自定义事件

    /*
     * 宿主即可以是 Activity(FragmentActivity(v4) 的子类) 也可以是 Fragment(v4) 类型
     */
    public void register(AppCompatActivity host) {
        // 无参无返回值
        mSettingReceiver.alive().lifecycle(host);
        // 无参有返回值
        mGetUserInfoReceiver.alive().lifecycle(host);
        // 有参无返回值
        mCallReceiver.alive().lifecycle(host);
        // 有参有返回值
        mConditionUserInfoReceiver.alive().lifecycle(host);
    }


**根据业务场景选择合适的事件类型即可**

---

##### 3. 注意事项

1. 注解 EventName 用于指定当前事件名称，在调用方（JSSDK-JavaScript 部分）使用。
2. 同一个 EventName 事件在内存中只能存在一份，JSSDK 框架将会根据该 EventName 匹配到唯一目标 Native 事件。
3. callback 机制在无 return 事件中提供，适用于异步或依赖用户事件的场景。

---

#### 二、JavaScript 调用到 Native 方法


Native 方法的定义要通过自定义桥梁来完成，并且用 @JavascriptInterface 来声明

##### 1. Native 端自定义方法

```
public final class TestBridge extends BaseBridge<X5WebViewActivity> {

   /**
    * Called after {@link BaseBridge#bindTarget(Object, WebView)}
    */
    @Override
    protected void onBind() {
        // 在 bindTarget 之后回调
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 在 X5WebViewActivity onDestroy 后回调
    }

    /*
     * 自定义方法
     */
    @JavascriptInterface
    public void goToSchool(String params){
        // 通过框架内部提供的 Request 实现反序列化，获取到传递参数
        final Request<ToSchool> request = Request.parseRequest(params, ToSchool.class);
        // 获取传递参数
        final ToSchool school = request.getParams();

        // do something

        // 通过框架提供的 Response 构建返回，返回到 JavaScript 调用者
        final Bean bean = new Bean();
        bean.state = 1;
        bean.desc = "到学校咯";
        Response.create(request.getCallbackId())
            .result(bean).notify(getWebView());

        // result 提供了自定义序列化 key，根据需要选择
        // Response.create(request.getCallbackId())
                    .result(bean, "custom").notify(getWebView());
    }
}
```

##### 2. JavaScript 端调用

JSSDK-JavaScript 可以直接根据原生定义的**方法名**进行调用，示例如下：

> 详情请参照 JSSDK-JavaScript 部分，或项目内 demo

```

lr.goToSchool({
    schoolName: '宾夕法尼亚大学',
    className: '计算机学院-',
    success: function(res){
    console.log('result: ' + JSON.stringify(res))
    },
    failed: function(){
    },
});

// 如果参数是原生数据类型
lr.goToSchool({
    params: '这里是原生数据类型：int、string、boolean...'
    success: function(res){
    console.log('result: ' + JSON.stringify(res))
    },
    failed: function(){
    },
});

    
```

---


### 第二部分：Native 调用 JavaScript 方法


Native 调用 JavaScript 方法同样提供了以上四种场景，即：

- 无参无返回值
- 无参有返回值
- 有参无返回值
- 有参有返回值

>**通过 Request 发起的调用 JavaScript 方法，将会根据当前 WebView 的生命周期自动管理 Request 的生命周期；即当前 WebView 移除后，该 WebView 匹配的相关 Request 也会随之被销魂**


如何通过 Request 构建调用 JavaScript 方法的过程。

**（1）无参无返回值**

    /**
     * 调用JavaScript-无参无返回值
     */
    private void callFromNativeNoParamsNoReturn() {
        // 通过 Request 构建调用 JavaScript 方法，‘callFromNativeNoParamsNoReturn’ 为要调用的 JavaScript 方法名称
        Request.call("callFromNativeNoParamsNoReturn").to(webView);
    }

**（2）有参无返回值**

    /**
     * 调用JavaScript-有参无返回值
     */
    private void callFromNativeWithParamsNoReturn() {
        final UserBean bean = new UserBean();
        bean.name = "调用JavaScript方法，携带参数";
        bean.age = 20;
        bean.sex = "boy";
        // 携带参数
        Request.call("callFromNativeWithParamsNoReturn").params(bean).to(webView);
    }

**（3） 无参有返回值**

    /**
     * 调用JavaScript-无参有返回值
     */
    private void callFromNativeNoParamsWithReturn() {
        Request.call("callFromNativeNoParamsWithReturn").to(webView);
    }

> 注意，调用 JavaScript 无参方法，由于无法获取到方法 id，此时无法实现返回值场景。

**（4）有参有返回值**

    /**
     * 调用JavaScript-有参有返回值
     */
    private void callFromNativeWithParamsWithReturn() {
        final UserBean bean = new UserBean();
        bean.name = "godliness";
        bean.age = 100;
        bean.sex = "boy";

        // 调用 JavaScript 的 callFromNativeWithParamsWithReturn 方法
        Request.call("callFromNativeWithParamsWithReturn")
                .params(bean)
                .callback(new ResultCallback<UserBean>() {
                    @Override
                    protected void onReceiveValue(Result<Bean> result) {
                        final Bean bean = result.getResult();
                        new AlertDialog.Builder(DemoActivity.this)
                                .setMessage("来自JavaScript的Return: " + bean.toString())
                                .setCancelable(true)
                                .show();
                    }
                }).to(webView);
    }
    
---
    
    
## 混淆配置

JSSDK 内部已经配置混淆，二次混淆可能会引起 bug，根据库的使用情况添加如下混淆过滤：

### JSSDK 混淆配置

    // 如果使用 jssdk 添加如下混淆过滤
    -keepnames class * extends com.longrise.android.jssdk.core.bridge.BaseBridge{
        @android.webkit.JavascriptInterface <methods>;
    }
    -keepnames class * extends com.longrise.android.jssdk.channel.JSEventChannel{
        @com.longrise.android.jssdk.annotation.JSEvent <methods>;
    }
    -keep class * extends com.longrise.android.jssdk.channel.JSEventListener


### JSSDK-X5 混淆配置
    
    // 如果使用 jssdk-x5 添加如下混淆过滤
    -keepnames class * extends com.longrise.android.jssdk_x5.core.bridge.BaseBridge{
        @android.webkit.JavascriptInterface <methods>;
    }
    -keepnames class * extends com.longrise.android.jssdk_x5.channel.JSEventChannel{
        @com.longrise.android.jssdk.annotation.JSEvent <methods>;
    }
    -keep class * extends com.longrise.android.jssdk_x5.channel.JSEventListener 

---


## 简单用例

> 详情请参照项目 demo 中的示例

### 1. JavaScript 调用原生事件


-  H5 端声明
    
```
    
    <!DOCTYPE html>
    <html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>callNative</title>

    <!-- 引入 lrBridgeCore.js -->
    <script type="text/javascript" src="js-sdk/core/lrBridgeCore.js"></script>
    <style type="text/css">
			button {
				width: 100%;
				height: 2.5rem;
				margin-top: 1rem;
			}

    </style>
    </head>
    
    <button type="button" onclick="getUserInfo()"> getUserInfo
    </button>
    
    <script>
        lr.config({
            eventList:[
                'getUserInfo'  // 配置自定义事件名，注意原生的声明
            ]
        });
        
        
        /**
         * 获取用户信息（无参树-有返回值）
         */
        function getUserInfo(){
            lr.getUserInfo({
                success: function(res){
                    console.log('用户信息: ' + JSON.stringify(res.result))
                },
                failed: function(res){
                    
                }
            });
        }
    </script>
    
    <body>
    </html>
```
    

- 原生声明


```
    /**
     * 声明无参数，有返回值类型的事件-获取用户信息
     */
    private final IReturnReceiver<UserInfo> mGetUserInfoReceiver = new IReturnReceiver<UserInfo>() {

        // 注意该 EventName 值与 JavaScript 中要一致
        @EventName("getUserInfo")
        @Override
        protected UserInfo onEvent() {
            final UserInfo info = new UserInfo();
            info.name = "godliness";
            info.age = 100;
            info.sex = "boy";
            return info;
        }
    };
    
    // 在Activity/Fragment 生命周期中注册该事件注册该事件
    mGetUserInfoReceiver.alive().lifecycle(this);
```    
    
    
### 2. 原生调用 JavaScript 方法

> 详情请参照项目 demo 中示例

- 声明 JavaScript 方法

```
    
    /**
     * 由原生调用-有参数有返回值
     */
    function fromNative(params){
        var obj = JSON.parse(params);
        console.log('来自原生的参数: ' + JSON.stringify(obj.params));

        // 返回给 Native 调用者
        lr.notifyNative({
            id: obj.id,
            result: {
                state: 1,
                desc: '调用成功'
            }
        });
    }
```

    
- 原生调用



```
    /**
     * 调用 JavaScript 有参数有返回值方法
     */
    private void callWPWR() {
        final UserInfo info = new UserInfo();
        info.name = "godliness";
        info.age = 100;
        info.sex = "boy";
        info.address = "china";
        info.company = "free";

        // 调用 JavaScript 的 fromNative 方法
        Request.call("callFromNativeWithParamsWithReturn")
                .params(info)
                .callback(new ResultCallback<Bean>() {
                    @Override
                    protected void onReceiveValue(Result<Bean> result) {
                        // 来自 JavaScript 的返回
                        final Bean response = result.getResult();
                        Log.i(TAG, "result: " + response.toString());
                    }
                }).to(mWebView);
    }
```