package com.longrise.android.jssdk.apt;

/**
 * Created by godliness on 2021/7/29.
 *
 * @author godliness
 */
final class EventConst {

    static String JS_EVENT_CHANNEL_SIMPLE_NAME = "JSEventChannel";

    static String JS_EVENT_CHANNEL_NAME = "com.longrise.android.jssdk.channel.JSEventChannel";

    static String X5_JS_EVENT_CHANNEL_NAME = "com.longrise.android.jssdk_x5.channel.JSEventChannel";

    static String JS_EVENT_LISTENER_NAME = "com.longrise.android.jssdk.channel.JSEventListener;";

    static String X5_JS_EVENT_LISTENER_NAME= "com.longrise.android.jssdk_x5.channel.JSEventListener;";

}
