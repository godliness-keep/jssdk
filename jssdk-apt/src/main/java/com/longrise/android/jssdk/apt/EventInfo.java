package com.longrise.android.jssdk.apt;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

/**
 * Created by godliness on 2021/7/26.
 *
 * @author godliness
 */
final class EventInfo {

    private static final HashSet<String> TYPE_CHECK = new HashSet<>(5);

    private final String packageName;
    private final String className;
    //    private final String simpleName;
    private final String event;
    private final String returnParam;
    private final String param;

    private boolean isGeneralityParam;
    private boolean isJSEventRT;
    private boolean isX5;

    EventInfo(ExecutableElement element, Types typeUtils) {
        // TypeElement -> PackageElement
//        this.packageName = element.getEnclosingElement().getEnclosingElement().asType().toString();
        this.packageName = getPackageName(element);
        this.className = element.getEnclosingElement().asType().toString();
//        this.simpleName = element.getEnclosingElement().getSimpleName().toString();
        this.event = element.getSimpleName().toString();
        this.returnParam = checkReturnParam(element);
        this.param = checkParam(element, typeUtils);

        checkTypeAccessControl(element);
        checkMethodAccessControl(element);
        checkJSEventRT(element, typeUtils);

//        System.out.println("==========> EventInfo: " + getPackageName(element) + " class: " + className);
    }

    private void checkJSEventRT(ExecutableElement element, Types types) {
        final Element typeEle = element.getEnclosingElement();
        if (typeEle.getKind() == ElementKind.CLASS) {
            final Element asElement = types.asElement(((TypeElement) typeEle).getSuperclass());
            if (asElement.getKind() == ElementKind.CLASS) {
                final String superName = ((TypeElement) asElement).getQualifiedName().toString();
                if (EventConst.JS_EVENT_CHANNEL_NAME.equals(superName)) {
                    isJSEventRT = true;
                    isX5 = false;
                } else if (EventConst.X5_JS_EVENT_CHANNEL_NAME.equals(superName)) {
                    isJSEventRT = true;
                    isX5 = true;
                } else {
                    isJSEventRT = false;
                }
            }
        }
    }

    private String getPackageName(Element element) {
        while (element.getKind() != ElementKind.PACKAGE) {
            element = element.getEnclosingElement();
        }
        return element.asType().toString();
    }

    private void checkTypeAccessControl(ExecutableElement element) {
        if (TYPE_CHECK.contains(className)) {
            return;
        }
        TYPE_CHECK.add(className);
        final Set<Modifier> modifiers = element.getEnclosingElement().getModifiers();
        boolean isPrivate = false;
        for (Modifier modifier : modifiers) {
            if (Modifier.PRIVATE.equals(modifier)) {
                isPrivate = true;
                break;
            }
        }
        if (isPrivate) {
            throw new IllegalStateException("\n" +
                    "The access control for the class where the JSEvent modifier resides must be greater than or equal to the package(default, protected, public) at \"" + className + "\"");
        }
    }

    private void checkMethodAccessControl(ExecutableElement element) {
        final Set<Modifier> modifiers = element.getModifiers();
        boolean isAccess = true;
        boolean isAbstract = false;
        for (Modifier modifier : modifiers) {
            if (modifier.equals(Modifier.PRIVATE)) {
                isAccess = false;
                break;
            }
            if (Modifier.ABSTRACT.equals(modifier)) {
                isAbstract = true;
                break;
            }
        }
        if (!isAccess) {
            throw new IllegalStateException("The JSEvent modifier must have access equal to or greater than the package (default, protected, public)，at \"" + event + "\" in " + className);
        }
        if (isAbstract) {
            throw new IllegalStateException("JSEvent cannot modify abstract methods，at \"" + event + "\" in " + className);
        }
    }

    String getPackage() {
        return packageName;
    }

    String getClassName() {
        return className;
    }

//    String getClassSimpleName() {
//        return simpleName;
//    }

    String getEvent() {
        return event;
    }

    boolean hasReturnParam() {
        return returnParam != null;
    }

    boolean hasParam() {
        return param != null;
    }

    String getParam() {
        return param;
    }

    boolean isGenericity() {
        return isGeneralityParam;
    }

    boolean isJSEventRT() {
        return isJSEventRT;
    }

    boolean isX5() {
        return isX5;
    }

    static void clean() {
        TYPE_CHECK.clear();
    }

    private String checkParam(ExecutableElement element, Types types) {
        final List<? extends VariableElement> params = element.getParameters();
        if (params.size() > 1) {
            throw new IllegalArgumentException("JSEvent supports method declarations with only one parameter, in \"" + element.getEnclosingElement().asType().toString() + "\" " + event);
        } else if (params.size() == 1) {
            final VariableElement variableElement = params.get(0);
            final TypeElement typeElement = (TypeElement) types.asElement(variableElement.asType());
            if (typeElement != null) {
                final List<? extends TypeParameterElement> typeParameterElements = typeElement.getTypeParameters();
                if (typeParameterElements != null && typeParameterElements.size() > 0) {
                    this.isGeneralityParam = true;
                }
            }
            return variableElement.asType().toString();
        }
        return null;
    }

    private String checkReturnParam(ExecutableElement element) {
        final TypeMirror mirror = element.getReturnType();
        if (TypeKind.VOID == mirror.getKind()) {
            return null;
        }
        return mirror.toString();
    }
}
