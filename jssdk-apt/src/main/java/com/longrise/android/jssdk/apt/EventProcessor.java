package com.longrise.android.jssdk.apt;


import com.longrise.android.jssdk.annotation.JSEvent;

import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;

public class EventProcessor extends AbstractProcessor {

    private Filer filer;
    private Types typeUtils;

    private HashMap<String, HashMap<String, EventInfo>> events;
    private HashMap<String, HashSet<String>> imports;

    private static final String NEW_LINE = "\n";
    private static final String TAB = "\t";
    private static final String NEW_LINE_TAB = "\n\t";
    private static final String semicolon = ";";

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        this.filer = processingEnvironment.getFiler();
        this.events = new HashMap<>();
        this.imports = new HashMap<>();
        this.typeUtils = processingEnv.getTypeUtils();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnv) {
        final Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(JSEvent.class);
        for (Element element : elements) {
            if (element.getKind() != ElementKind.METHOD) {
                throw new IllegalArgumentException("JSEvent can only modify methods.");
            }

            final EventInfo info = new EventInfo((ExecutableElement) element, typeUtils);
            if (!info.isJSEventRT()) {
                throw new IllegalStateException("JSEvent can only be used in " + EventConst.JS_EVENT_CHANNEL_SIMPLE_NAME + ": at \"" + info.getEvent() + "\" method in " + info.getClassName());
            }

            if (info.isGenericity()) {
                addImport(info.getClassName(), "com.google.gson.reflect.TypeToken;");
            }
            if (info.isX5()) {
                addImport(info.getClassName(), EventConst.X5_JS_EVENT_LISTENER_NAME);
            } else {
                addImport(info.getClassName(), EventConst.JS_EVENT_LISTENER_NAME);
            }
            addEvent(info.getClassName(), info);
        }

        if (events.isEmpty() && imports.isEmpty()) {
            return true;
        }

        System.out.println("-------> size: " + events.size() + " imports size: " + imports.size());

        for (Map.Entry<String, HashMap<String, EventInfo>> entry : events.entrySet()) {
            final String className = entry.getKey();
            final String packageName = getPackageName(entry.getValue(), className);
            Writer writer = null;
            try {
                writer = createHeader(packageName, className, imports.get(className));
                writeBody(writer, className, entry.getValue());
                writer.flush();
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                EventInfo.clean();
            }
        }

        return true;
    }

    private String getPackageName(HashMap<String, EventInfo> eventInfos, String className) {
        for (Map.Entry<String, EventInfo> item : eventInfos.entrySet()) {
            return item.getValue().getPackage();
        }
        throw new IllegalStateException("The package name was not obtained in: " + className);
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(JSEvent.class.getCanonicalName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_7;
    }

    private void addImport(String name, String item) {
        HashSet<String> importList = imports.get(name);
        if (importList == null) {
            imports.put(name, importList = new HashSet<>());
        }
        importList.add(item);
    }

    private void addEvent(String name, EventInfo info) {
        HashMap<String, EventInfo> infos = events.get(name);
        if (infos == null) {
            events.put(name, infos = new HashMap<>());
        }
        final String event = info.getEvent();
        if (infos.get(event) != null) {
            throw new IllegalStateException("There cannot be duplicate event names \"" + event + "\" in " + name);
        }
        infos.put(event, info);
    }

    private Writer createHeader(String packageName, String className, HashSet<String> imports) throws IOException {
        String simpleClassName = className.replace(packageName + ".", "");
        simpleClassName = simpleClassName.replace(".", "$");
        JavaFileObject jfo = filer.createSourceFile(packageName + "." + simpleClassName + "$Events");
        final Writer writer = jfo.openWriter();
        writer.write("package " + packageName + semicolon);
        writer.write(NEW_LINE + NEW_LINE);
        writer.write("import android.support.annotation.NonNull;");
        writer.write(NEW_LINE);
        if (imports != null) {
            for (String importStr : imports) {
                writer.write("import " + importStr);
                writer.write(NEW_LINE);
            }
        }
        writer.write(NEW_LINE);
        writer.write("public final class " + simpleClassName + "$Events implements JSEventListener<" + className + ">{");
        writer.write(NEW_LINE);
        return writer;
    }

    private void writeBody(Writer writer, String className, HashMap<String, EventInfo> eventInfos) throws IOException {
        final int eventSize = eventInfos.size();

        writer.write(NEW_LINE_TAB);
        writer.write("private final String[] mEvents;");
        writer.write(NEW_LINE + NEW_LINE + TAB);
        writer.write("{");
        writer.write(NEW_LINE_TAB + TAB);
        writer.write("mEvents = new String[" + eventSize + "];");
        int index = 0;
        for (Map.Entry<String, EventInfo> item : eventInfos.entrySet()) {
            final EventInfo info = item.getValue();
            writer.write(NEW_LINE + TAB + TAB);
            writer.write("mEvents[" + index + "] = \"" + info.getEvent() + "\";");
            index++;
        }

        writer.write(NEW_LINE + TAB);
        writer.write("}");

        writer.write(NEW_LINE + NEW_LINE + TAB);
        writer.write("@NonNull\n" +
                "\t@Override\n" +
                "\tpublic String[] getEvents() {\n" +
                "        return mEvents;\n" +
                "    }");
        writer.write(NEW_LINE + NEW_LINE + TAB);
        writer.write("@Override\n" +
                "    public void invoke(@NonNull String eventName, @NonNull String params, @NonNull " + className + " er) {");

        writeInvoke(writer, eventInfos);
        writer.write(NEW_LINE + TAB + "}");

        writer.write(NEW_LINE);
        writer.write("}");
    }

    private void writeInvoke(Writer writer, HashMap<String, EventInfo> eventInfos) throws IOException {
        writer.write(NEW_LINE + TAB + TAB);
        writer.write("switch (eventName) {");
        for (Map.Entry<String, EventInfo> item : eventInfos.entrySet()) {
            final EventInfo eventInfo = item.getValue();
            final String eventName = eventInfo.getEvent();
            writer.write(NEW_LINE + TAB + TAB + TAB);
            writer.write("case \"" + eventName + "\":");
            writer.write(NEW_LINE + TAB + TAB + TAB + TAB);
            if (eventInfo.hasParam() && eventInfo.hasReturnParam()) {
                if (eventInfo.isGenericity()) {
                    writeParseTypeParams(writer, eventName, eventInfo.getParam());
                } else {
                    writeParseParams(writer, eventName, eventInfo.getParam());
                }
                writer.write(NEW_LINE + TAB + TAB + TAB + TAB);
                writer.write("er.callback(er." + eventName + "(" + eventName + ")" + ");");
            } else if (eventInfo.hasParam()) {
                if (eventInfo.isGenericity()) {
                    writeParseTypeParams(writer, eventName, eventInfo.getParam());
                } else {
                    writeParseParams(writer, eventName, eventInfo.getParam());
                }
                writer.write(NEW_LINE + TAB + TAB + TAB + TAB);
                writer.write("er." + eventName + "(" + eventName + ");");
            } else if (eventInfo.hasReturnParam()) {
                writer.write("er.callback(er." + eventName + "()" + ");");
            } else {
                writer.write("er." + eventName + "();");
            }
            writer.write(NEW_LINE + TAB + TAB + TAB + TAB + "break;");
        }

        writer.write(NEW_LINE + TAB + TAB + TAB);
        writer.write("default:\n" +
                "                break;");
        writer.write(NEW_LINE + TAB + TAB + "}");
    }

    private void writeParseTypeParams(Writer writer, String eventName, String param) throws IOException {
        writer.write("final " + param + " " + eventName + " = "
                + "er.parseParams(params, new TypeToken<" + param + ">() {\n" +
                "                }.getType());");
    }

    private void writeParseParams(Writer writer, String eventName, String param) throws IOException {
        writer.write("final " + param + " " + eventName + " = "
                + "er.parseParams(params, " + param + ".class);");
    }
}