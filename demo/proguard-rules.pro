# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-printconfiguration configuration.txt


#================================================
#             JSSDK
#================================================
-keepnames class * extends com.longrise.android.jssdk.channel.JSEventChannel{
    @com.longrise.android.jssdk.annotation.JSEvent <methods>;
}
-keep class * extends com.longrise.android.jssdk.channel.JSEventListener

#================================================
#           JSSDK-X5
#================================================
-keepnames class * extends com.longrise.android.jssdk_x5.channel.JSEventChannel{
    @com.longrise.android.jssdk.annotation.JSEvent <methods>;
}
-keep class * extends com.longrise.android.jssdk_x5.channel.JSEventListener


-keep class com.longrise.android.jssdk.demo.mode.UserInfo{
    *;
}