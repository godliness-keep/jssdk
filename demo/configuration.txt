# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html
#
# Starting with version 2.2 of the Android plugin for Gradle, this file is distributed together with
# the plugin and unpacked at build-time. The files in $ANDROID_HOME are no longer maintained and
# will be ignored by new version of the Android plugin for Gradle.

# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize steps (and performs some
# of these optimizations on its own).
# Note that if you want to enable optimization, you cannot just
# include optimization flags in your own project configuration file;
# instead you will need to point to the
# "proguard-android-optimize.txt" file instead of this one from your
# project.properties file.
-dontoptimize

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

# Preserve some attributes that may be required for reflection.
-keepattributes *Annotation*,Signature,InnerClasses,EnclosingMethod

-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
-keep public class com.google.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService
-dontnote com.google.vending.licensing.ILicensingService
-dontnote com.google.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# Keep setters in Views so that animations can still work.
-keepclassmembers public class * extends android.view.View {
    void set*(***);
    *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick.
-keepclassmembers class * extends android.app.Activity {
    public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# Preserve annotated Javascript interface methods.
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# The support libraries contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontnote android.support.**
-dontnote androidx.**
-dontwarn android.support.**
-dontwarn androidx.**

# This class is deprecated, but remains for backward compatibility.
-dontwarn android.util.FloatMath

# Understand the @Keep support annotation.
-keep class android.support.annotation.Keep
-keep class androidx.annotation.Keep

-keep @android.support.annotation.Keep class * {*;}
-keep @androidx.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <init>(...);
}

# These classes are duplicated between android.jar and org.apache.http.legacy.jar.
-dontnote org.apache.http.**
-dontnote android.net.http.**

# These classes are duplicated between android.jar and core-lambda-stubs.jar.
-dontnote java.lang.invoke.**

# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-printconfiguration configuration.txt


#================================================
#             JSSDK
#================================================
-keepnames class * extends com.longrise.android.jssdk.channel.JSEventChannel{
    @com.longrise.android.jssdk.annotation.JSEvent <methods>;
}
-keep class * extends com.longrise.android.jssdk.channel.JSEventListener

#================================================
#           JSSDK-X5
#================================================
-keepnames class * extends com.longrise.android.jssdk_x5.channel.JSEventChannel{
    @com.longrise.android.jssdk.annotation.JSEvent <methods>;
}
-keep class * extends com.longrise.android.jssdk_x5.channel.JSEventListener


-keep class com.longrise.android.jssdk.demo.mode.UserInfo{
    *;
}
# Referenced at /Users/sujizhong/Documents/Longrise/分层架构设计/业务组件层/JSSDK/demo/build/intermediates/merged_manifests/debug/AndroidManifest.xml:11
-keep class android.support.v4.app.CoreComponentFactory { <init>(); }
# Referenced at /Users/sujizhong/Documents/Longrise/分层架构设计/业务组件层/JSSDK/demo/build/intermediates/merged_manifests/debug/AndroidManifest.xml:20
-keep class com.longrise.android.jssdk.demo.MainActivity { <init>(); }
# Referenced at /Users/sujizhong/Documents/Longrise/分层架构设计/业务组件层/JSSDK/demo/build/intermediates/merged_manifests/debug/AndroidManifest.xml:27
-keep class com.longrise.android.jssdk.demo.raw.WebViewActivity { <init>(); }
# Referenced at /Users/sujizhong/Documents/Longrise/分层架构设计/业务组件层/JSSDK/demo/build/intermediates/merged_manifests/debug/AndroidManifest.xml:30
-keep class com.longrise.android.jssdk.demo.x5.X5WebViewActivity { <init>(); }
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_alert_dialog_material.xml:41
-keep class android.support.v4.widget.NestedScrollView { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_select_dialog_material.xml:23
-keep class android.support.v7.app.AlertController$RecycleListView { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_action_menu_item_layout.xml:17
-keep class android.support.v7.view.menu.ActionMenuItemView { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_expanded_menu_layout.xml:17
-keep class android.support.v7.view.menu.ExpandedMenuView { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_popup_menu_item_layout.xml:17
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_list_menu_item_layout.xml:17
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_cascading_menu_item_layout.xml:20
-keep class android.support.v7.view.menu.ListMenuItemView { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_toolbar.xml:27
-keep class android.support.v7.widget.ActionBarContainer { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_toolbar.xml:43
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_action_mode_bar.xml:19
-keep class android.support.v7.widget.ActionBarContextView { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_toolbar.xml:17
-keep class android.support.v7.widget.ActionBarOverlayLayout { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_action_menu_layout.xml:17
-keep class android.support.v7.widget.ActionMenuView { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_activity_chooser_view.xml:19
-keep class android.support.v7.widget.ActivityChooserView$InnerLayout { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_alert_dialog_material.xml:18
-keep class android.support.v7.widget.AlertDialogLayout { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_alert_dialog_button_bar_material.xml:26
-keep class android.support.v7.widget.ButtonBarLayout { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_content_include.xml:19
-keep class android.support.v7.widget.ContentFrameLayout { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_alert_dialog_title_material.xml:45
-keep class android.support.v7.widget.DialogTitle { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_simple_overlay_action_mode.xml:23
-keep class android.support.v7.widget.FitWindowsFrameLayout { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_simple.xml:17
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_dialog_title_material.xml:22
-keep class android.support.v7.widget.FitWindowsLinearLayout { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_search_view.xml:75
-keep class android.support.v7.widget.SearchView$SearchAutoComplete { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_toolbar.xml:36
-keep class android.support.v7.widget.Toolbar { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_simple_overlay_action_mode.xml:32
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_screen_simple.xml:25
-keep class android.support.v7.widget.ViewStubCompat { <init>(...); }

# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_alert_dialog_title_material.xml:56
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_alert_dialog_material.xml:52
# Referenced at /Users/sujizhong/.gradle/caches/transforms-2/files-2.1/f61ff016bf088d92d3983698fc1fe6c1/res/layout/abc_alert_dialog_button_bar_material.xml:43
-keep class android.widget.Space { <init>(...); }

# Referenced at /Users/sujizhong/Documents/Longrise/分层架构设计/业务组件层/JSSDK/demo/src/main/res/layout/activity_x5webview.xml:55
-keep class com.tencent.smtt.sdk.WebView { <init>(...); }


# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Ensure that reflectively-loaded inflater is not obfuscated. This can be
# removed when we stop supporting AAPT1 builds.
-keepnames class android.support.v7.app.AppCompatViewInflater

# aapt is not able to read app::actionViewClass and app:actionProviderClass to produce proguard
# keep rules. Add a commonly used SearchView to the keep list until b/109831488 is resolved.
-keep class android.support.v7.widget.SearchView { <init>(...); }
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# keep setters in VectorDrawables so that animations can still work.
-keepclassmembers class android.support.graphics.drawable.VectorDrawableCompat$* {
   void set*(***);
   *** get*();
}

# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# CoordinatorLayout resolves the behaviors of its child components with reflection.
-keep public class * extends android.support.design.widget.CoordinatorLayout$Behavior {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>();
}

# Make sure we keep annotations for CoordinatorLayout's DefaultBehavior and ViewPager's DecorView
-keepattributes *Annotation*

# aapt2 is not (yet) keeping FQCNs defined in the appComponentFactory <application> attribute
-keep class android.support.v4.app.CoreComponentFactory

-keep public class * extends androidx.versionedparcelable.VersionedParcelable
-keep public class android.support.**Parcelizer { *; }
-keep public class androidx.**Parcelizer { *; }
-keep public class androidx.versionedparcelable.ParcelImpl

-keepattributes *Annotation*

-keepclassmembers enum android.arch.lifecycle.Lifecycle$Event {
    <fields>;
}

-keep class * implements android.arch.lifecycle.LifecycleObserver {
}

-keep class * implements android.arch.lifecycle.GeneratedAdapter {
    <init>(...);
}

-keepclassmembers class ** {
    @android.arch.lifecycle.OnLifecycleEvent *;
}
-keep class * extends android.arch.lifecycle.ViewModel {
    <init>();
}
-keep @android.support.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}

-ignorewarnings