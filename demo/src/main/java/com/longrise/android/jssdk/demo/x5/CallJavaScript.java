package com.longrise.android.jssdk.demo.x5;


import android.util.Log;

import com.longrise.android.jssdk.demo.mode.UserInfo;
import com.longrise.android.jssdk_x5.Request;
import com.longrise.android.jssdk_x5.core.protocol.Result;
import com.longrise.android.jssdk_x5.sender.ResultCallback;
import com.tencent.smtt.sdk.WebView;

/**
 * Created by godliness on 2021/7/27.
 *
 * @author godliness
 */
final class CallJavaScript {

    private static final String TAG = "CallJavaScript";

    /**
     * 调用 JavaScript initState 方法，JavaScript 端参照 sample.html
     */
    static void callInitState(WebView view) {
        Request.call("initState_").to(view);
    }

    /**
     * 调用 JavaScript setUserInfo，JavaScript 端参照 sample.html
     */
    static void callSetUserInfo(WebView view) {
        // 由于无参数类型，在 JavaScript 无法获取 id 标识，故无法实现调用无参的 JavaScript 并携带返回值的场景
        UserInfo info = new UserInfo();
        info.name = "godliness";
        info.age = 22;
        info.address = "china";
        info.sex = "boy";
        info.company = "IT";
        Request.call("setUserInfo_").params(info).to(view);
    }

    /**
     * 调用 JavaScript getUserInfo，JavaScript 端参照 sample.html
     */
    static void callGetUserInfo(WebView view) {
        Request.call("getUserInfo_")
                .params("")
                .callback(new ResultCallback<UserInfo>() {
                    @Override
                    protected void onReceiveValue(Result<UserInfo> result) {
                        Log.e(TAG, "用户信息：" + result.getResult().toString());
                    }
                }).to(view);
    }

    /**
     * 调用 JavaScript toPay，JavaScript 端参照 sample.html
     */
    static void callToPay(WebView view) {
        final int value = 100;

        // 调用 JavaScript 的 fromNative 方法
        Request.call("toPay_")
                .params(value)
                .callback(new ResultCallback<Boolean>() {
                    @Override
                    protected void onReceiveValue(Result<Boolean> result) {
                        Log.e(TAG, "支付结果：" + result.getResult());
                    }
                }).to(view);
    }
}
