package com.longrise.android.jssdk.demo.x5;

import android.app.Activity;
import android.util.Log;

import com.longrise.android.jssdk.demo.mode.UserInfo;
import com.longrise.android.jssdk_x5.receiver.IParamsReceiver;
import com.longrise.android.jssdk_x5.receiver.IParamsReturnReceiver;
import com.longrise.android.jssdk_x5.receiver.IReceiver;
import com.longrise.android.jssdk_x5.receiver.IReturnReceiver;
import com.longrise.android.jssdk_x5.receiver.base.EventName;

/**
 * Created by godliness on 2021/7/27.
 *
 * @author godliness
 */
final class JavaScriptX5SingleEvent {

    private static final String TAG = "SingleEvent";

    void register(Activity host) {
        mInitState.alive().lifecycle(host);
        mSetUserInfo.alive().lifecycle(host);
        mGetUserInfo.alive().lifecycle(host);
        mToPay.alive().lifecycle(host);
    }

    /**
     * 无参数无返回值的接收者
     */
    final IReceiver mInitState = new IReceiver() {
        @EventName("initState")
        @Override
        protected void onEvent() {
            Log.e(TAG, "initState");

            // 通知调用者状态初始化完毕
            // 根据业务场景选择是需要 return/callback
            callback("状态初始化完毕");
        }
    };

    /**
     * 含有参数的事件接收者
     */
    final IParamsReceiver<UserInfo> mSetUserInfo = new IParamsReceiver<UserInfo>() {
        @EventName("setUserInfo")
        @Override
        protected void onEvent(UserInfo params) {
            Log.e(TAG, "用户信息：" + params.toString());
        }
    };

    /**
     * 含有返回值的接收者
     */
    final IReturnReceiver<UserInfo> mGetUserInfo = new IReturnReceiver<UserInfo>() {
        @EventName("getUserInfo")
        @Override
        protected UserInfo onEvent() {
            UserInfo info = new UserInfo();
            info.name = "godliness";
            info.age = 22;
            info.address = "china";
            info.sex = "boy";
            info.company = "IT";
            return info;
        }
    };

    /**
     * 含有参数含有返回值的接收者
     */
    final IParamsReturnReceiver<Integer, Boolean> mToPay = new IParamsReturnReceiver<Integer, Boolean>() {
        @EventName("toPay")
        @Override
        protected Boolean onEvent(Integer params) {
            Log.e(TAG, "要支付的金额是：" + params);
            return true;
        }
    };
}
