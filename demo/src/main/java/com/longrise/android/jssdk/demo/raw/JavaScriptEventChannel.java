package com.longrise.android.jssdk.demo.raw;

import android.util.Log;

import com.longrise.android.jssdk.annotation.JSEvent;
import com.longrise.android.jssdk.demo.mode.UserInfo;
import com.longrise.android.jssdk.channel.JSEventChannel;

/**
 * Created by godliness on 2021/7/27.
 *
 * @author godliness
 * 多事件注册示例
 * <p>
 * 通过 JSEvent 声明的方法，其方法名就是事件名
 */
final class JavaScriptEventChannel extends JSEventChannel<JavaScriptEventChannel> {

    private static final String TAG = "EventChannel";

    @JSEvent
    void initState() {
        Log.e(TAG, "initState");

        // 通知调用者状态初始化完毕
        // 根据业务场景选择是需要 return/callback
        callback("状态初始化完毕");
    }

    @JSEvent
    void setUserInfo(UserInfo userInfo) {
        Log.e(TAG, "用户信息：" + userInfo.toString());
    }

    @JSEvent
    UserInfo getUserInfo() {
        UserInfo info = new UserInfo();
        info.name = "godliness";
        info.age = 22;
        info.address = "china";
        info.sex = "boy";
        info.company = "IT";
        return info;
    }

    @JSEvent
    boolean toPay(int money) {
        Log.e(TAG, "要支付的金额是：" + money);
        return true;
    }
}
