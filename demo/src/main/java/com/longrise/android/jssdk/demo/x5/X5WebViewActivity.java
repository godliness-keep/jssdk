package com.longrise.android.jssdk.demo.x5;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.longrise.android.jssdk.demo.R;
import com.longrise.android.jssdk_x5.core.bridge.BaseBridge;
import com.tencent.smtt.sdk.WebView;

/**
 * Created by godliness on 2021/4/20.
 *
 * @author godliness
 * 基于 Tencent TBS-WebView，注意导包 jssdk-x5
 */
public final class X5WebViewActivity extends AppCompatActivity {

    private WebView mWebView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_x5webview);
        mWebView = findViewById(R.id.x5webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("file:///android_asset/sample.html");

        bindJSSDK();
        registerEvent();
        initCallJS();
    }

    /*******************************************************
     *
     *    todo JavaScript 调用 Native 事件只需要 3 步
     *
     ********************************************************/

    /**
     * todo step 1、扩展 BaseBridge
     */
    static final class TestBridge extends BaseBridge<X5WebViewActivity> {

        /**
         * Called after {@link BaseBridge#bindTarget(Object, WebView)}
         */
        @Override
        protected void onBind() {
            // 在 bindTarget 之后回调
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            // 在 X5WebViewActivity onDestroy 后回调
        }
    }

    /**
     * todo step 2、绑定到 WebView
     */
    private void bindJSSDK() {
        final TestBridge x5Bridge = new TestBridge();
        x5Bridge.bindTarget(this, mWebView);
    }

    /**
     * todo step 3、注册由 JavaScript 调用的事件
     */
    private void registerEvent() {
        // todo 实现方式 1、通过 JSEvent
        new JavaScriptX5EventChannel().alive().lifecycle(this);

        // todo 实现方式2、通过 EventName
        // 同一个宿主不允许相同的事件名，故注释掉
        // 使用者通过分别测试查看运行效果
//        new JavaScriptX5SingleEvent().register(this);
    }

    /*******************************************************
     *
     *    todo Native 调用 JavaScript 方法只需 1 步
     *
     ********************************************************/

    private void initCallJS() {
        findViewById(R.id.initState).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallJavaScript.callInitState(mWebView);
            }
        });
        findViewById(R.id.setUserInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallJavaScript.callSetUserInfo(mWebView);
            }
        });
        findViewById(R.id.getUserInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallJavaScript.callGetUserInfo(mWebView);
            }
        });
        findViewById(R.id.toPay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallJavaScript.callToPay(mWebView);
            }
        });
    }
}
