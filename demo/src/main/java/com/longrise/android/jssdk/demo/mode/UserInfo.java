package com.longrise.android.jssdk.demo.mode;

/**
 * Created by godliness on 2021/4/20.
 *
 * @author godliness
 */
public final class UserInfo {

    public String name;

    public int age;

    public String sex;

    public String address;

    public String company;

    @Override
    public String toString() {
        return "UserInfo{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", address='" + address + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
