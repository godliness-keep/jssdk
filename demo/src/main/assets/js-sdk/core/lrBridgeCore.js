var lr = (function() {
	var path = null;
	var scripts = document.getElementsByTagName("script")
	var isHC = false;
	for (var script of scripts) {
		isHC = false;
		var src = script.getAttribute("src");
		if (!src) {
			src = script.getAttribute("path");
			isHC = true;
		}
		if (src) {
			var index = src.indexOf("?");
			if (index > 0) {
				var pathTemp = src.substring(0, index);
				if (pathTemp.lastIndexOf("lrBridgeCore.js") >= 0) {
					if (isHC) {
						path = '/' + window.location.pathname.split('/')[1] + '/' + pathTemp;
					} else {
						path = pathTemp;
					}
					break
				}
			} else {
				if (src.lastIndexOf("lrBridgeCore.js") >= 0) {
					if (isHC) {
						path = '/' + window.location.pathname.split('/')[1] + '/' + src;
					} else {
						path = src;
					}
					break
				}
			}
		}
	}

	if (!path) {
		throw 'Failed to parse to the js-sdk path';
	}

	function uriParser() {
		return path.substring(0, path.indexOf('core/lrBridgeCore.js'));
	}

	var lazyLoader = (function(sdkPointer) {

		var moduleList = {};
		var sdkPath = null;

		function LoaderHelper(moduleName, callback) {
			this.moduleName = moduleName;
			this.callback = callback;

			if (!sdkPath) {
				sdkPath = sdkPointer();
			}
		}

		LoaderHelper.prototype = {

			load: function() {
				this.moduleName = sdkPath + this.moduleName;
				if (this.isLoad()) {
					this.callback();
				} else {
					var script = document.createElement('script');
					script.src = this.moduleName;
					script.type = 'text/javascript';
					document.getElementsByTagName('head')[0].appendChild(script)

					var that = this;
					script.onload = function() {
						moduleList[that.moduleName] = that.moduleName
						that.callback();
					}
				}
			},

			isLoad: function() {
				var key = this.moduleName;
				return typeof moduleList[key] !== 'undefined' && moduleList[key] === key
			}
		}

		return {
			load: function(moduleName, callback) {
				var loaderHelper = new LoaderHelper(moduleName, callback);
				loaderHelper.load();
			}
		}

	})(uriParser)

	var JAVASCRIPT_CALL_FINISHED = 'onJavaScriptCallFinished'
	var CALL_NATIVE_FROM_JAVASCRIPT = 'callNativeFromJavaScript'
	var RESULT_OK = 1

	var jsVersion = 1
	var jsCallbacks = {}
	var jsCallbackCurrentId = -1
	var isReady = false

	function sendNativeInternal(mapObject, message) {
		var hasParams = (message.params != null ||
			message.eventName != null ||
			message.success != null ||
			message.failed != null)

		var request = null;

		if (hasParams) {
			request = packageRequest(message)
		}

		lazyLoader.load('utils/platform.js', () => {
			if (isAndroid()) {
				sendToAndroid(mapObject, message.methodName, request)
			} else if (isIos()) {
				sendToiOS(message.methodName, request)
			} else {
				alert('Unknown platform');
			}
		});
	}

	function sendToAndroid(mapObject, methodName, request) {
		if (request != null) {
			mapObject[methodName](JSON.stringify(request))
		} else {
			mapObject[methodName]()
		}
	}

	function sendToiOS(methodName, request) {
		if (request != null) {
			webkit.messageHandlers[methodName].postMessage(JSON.stringify(request));
		} else {
			webkit.messageHandlers[methodName].postMessage(null);
		}
	}

	function callNativeInternal(mapObject, message) {
		sendNativeInternal(mapObject, message);
	}

	function callEventInternal(mapObject, message) {
		message.methodName = CALL_NATIVE_FROM_JAVASCRIPT
		sendNativeInternal(mapObject, message)
	}

	function notifyNativeInternal(mapObject, message) {
		var response = {
			version: jsVersion,
			id: message.id,
		}

		response.result = generateNotifyBody(message.params);

		lazyLoader.load('utils/platform.js', () => {
			if (isAndroid()) {
				sendToAndroid(mapObject, JAVASCRIPT_CALL_FINISHED, response);
			} else if (isIos()) {
				sendToiOS(JAVASCRIPT_CALL_FINISHED, response);
			} else {
				alert('Unknown platform');
			}
		});
	}

	function onBridgeConfigInternal(message) {
		if (!isReady) {
			ready();
		}
		lazyLoader.load('core/bridging.js', () => {
			bridging.config(lr, message)
		});
	}

	function dispatchReceiverInternal(request) {
		lazyLoader.load('core/receiver.js', () => {
			receiverManager.dispatchReceiver(request)
		});
	}

	function onNativeCallFinishedInternal(response) {
		lazyLoader.load('core/chunkParse.js', () => {
			if (chunkParse.onChunk(response)) {
				return
			}
			onNativeCallFinished(response);
		});
	}

	function onNativeCallFinished(response) {
		var responseJson = null;

		if (response && typeof response === 'object') {
			responseJson = response;
		} else {
			responseJson = JSON.parse(response);
		}

		var protocolId = responseJson.id;
		var protocolVersion = responseJson.version;

		var callback = jsCallbacks[protocolId];
		if (!callback || typeof callback !== 'object') {
			return;
		}

		var result = responseJson.result;

		try {
			if (result.state === RESULT_OK) {
				onSucceedCallback(callback.sck, result);
			} else {
				onFailedCallback(callback.fck, result);
			}
		} finally {
//			delete jsCallbacks[protocolId];
			onCompletedCallback(callback.cck);
		}
	}

	function onSucceedCallback(sck, result) {
		if (sck && typeof sck === 'function') {
			if (sck.length > 0) {
				sck(result);
			} else {
				sck();
			}
		}
	}

	function onFailedCallback(fck, result) {
		if (fck && typeof fck === 'function') {
			if (fck.length > 0) {
				fck(result);
			} else {
				fck();
			}
		}
	}

	function onCompletedCallback(cck) {
		if (cck && typeof cck === 'function') {
			cck()
		}
	}

	function packageRequest(message) {
		var request = {
			version: jsVersion,
			eventName: message.eventName,
			params: message.params
		}
		if (typeof message.success === 'function' ||
			typeof message.failed === 'function' ||
			typeof message.complete === 'function') {

			request.id = ++jsCallbackCurrentId

			var chain = {
				sck: message.success,
				fck: message.failed,
				cck: message.complete
			}

			jsCallbacks[request.id] = chain;
		}
		return request
	}

	function addMethod(object, name, fn) {
		var old = object[name];
		object[name] = function() {
			if (fn.length === arguments.length) {
				return fn.apply(this, arguments);
			} else if (typeof old === 'function') {
				return old.apply(this, arguments);
			}
		}
	}

	function generateNotifyBody(params) {
		if (params && typeof params === 'object') {
			if (params.state != null &&
				params.desc != null &&
				params.result != null &&
				Object.getOwnPropertyNames(params).length === 3) {
				// 属于标准的响应体
				return params;
			}
			return {
				state: typeof params.state === 'number' ? params.state : 0,
				desc: typeof params.desc === 'string' ? params.desc : 'unknown',
				result: params
			}
		}
		return {
			state: 0,
			desc: 'unknown',
			result: params
		};
	}

	function ready() {

		// already
		isReady = true;

		/**
		 * @param {string} message.methodName - Native 方法名称，必填
		 * @param {object} message.params - Native 方法参数，选填（无参类型方法）
		 * @param {function} message.success - Native 回调（成功回调，取决于业务）选填
		 * @param {failed} message.failed - Native 回调（失败回调，取决于业务）选填
		 * */
		addMethod(lr, 'callNative', function(message) {
			if (typeof lrBridge === 'undefined') {
				lrBridge = null;
			}
			lr.callNative(lrBridge, message);
		})

		/**
		 * @param {object} mapObject 来自原生的映射对象（Android可能会需要，iOS在使用WkWebView时则不需要）
		 * @param {object} message 参照上面方法message
		 * */
		addMethod(lr, 'callNative', function(mapObject, message) {
			callNativeInternal(mapObject, message);
		})

		/**
		 * @param {string} message.eventName - Native 事件名称，必填
		 * @param {object} message.params - Native 事件方法参数，选填（无参数类型）
		 * @param {function} message.success - Native 回调（成功回调，取决于业务）选填
		 * @param {function} message.failed - Native 回调（失败回调，取决于业务）选填
		 * */
		addMethod(lr, 'callEvent', function(message) {
			if (typeof lrBridge === 'undefined') {
				lrBridge = null;
			}
			lr.callEvent(lrBridge, message);
		})

		/**
		 * @param {object} mapObject 来自原生的映射对象（Android可能会需要，iOS在使用WkWebView时则不需要）
		 * @param {object} message 参照上面方法message
		 * */
		addMethod(lr, 'callEvent', function(mapObject, message) {
			callEventInternal(mapObject, message);
		})

		/**
		 * @param {int} message.id - Native 回调id，必填
		 * @param {object} message.result - 返回值，选填
		 * 	{
		 * 		@param {int} message.result.state - 状态，必填
		 * 		@param {string} message.result.desc - 说明，必填
		 * 		@param {any} message.result.result - 返回参数，必填
		 * 	}
		 * */
		addMethod(lr, 'notifyNative', function(message) {
			if (typeof lrBridge === 'undefined') {
				lrBridge = null;
			}
			lr.notifyNative(lrBridge, message)
		})

		/**
		 * @param {object} mapObject 来自原生的映射对象（Android可能会需要，iOS在使用WkWebView时则不需要）
		 * @param {object} message 参照上面方法message
		 * */
		addMethod(lr, 'notifyNative', function(mapObject, message) {
			notifyNativeInternal(mapObject, message)
		})
	}

	return {

		config: function(message) {
			onBridgeConfigInternal(message);
		},

		ready: function() {
			if (!isReady) {
				ready();
			}
		},

		dispatchCallNativeCallback: function(response) {
			onNativeCallFinishedInternal(response)
		},

		dispatchReceiver: function(request) {
			dispatchReceiverInternal(request);
		}
	}
})();

/**
 * 接收来自Native的通知, Native返回Response
 * */
function onNativeCallFinished(response) {
	lr.dispatchCallNativeCallback(response)
}

/**
 * 接收来自Native的事件调用
 * */
function callJavaScriptFromNative(request) {
	lr.dispatchReceiver(JSON.parse(request))
}
