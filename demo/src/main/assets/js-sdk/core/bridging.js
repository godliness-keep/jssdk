if (typeof bridging === 'undefined') {

	var bridging = (function() {

		function addMethod(core, methodName) {
			if (typeof core[methodName] === 'function') {
				return
			}
			core[methodName] = function(message) {
				var success = message.success;
				var failed = message.failed;
				var complete = message.complete;

				delete message.success;
				delete message.failed;
				delete message.complete;

				if (message) {
					var params = null;
					if (message.hasOwnProperty('params')) {
						if (Object.getOwnPropertyNames(message).length === 1) {
							params = message.params;
						} else {
							params = message;
						}
					} else {
						params = message;
					}

					lr.callNative({
						methodName: methodName,
						params: params,
						success: success,
						failed: failed,
						complete: complete
					});
				} else {
					lr.callNative({
						methodName: methodName,
					});
				}
			}
		}

		function addEvent(core, eventName) {
			if (typeof core[eventName] === 'function') {
				return;
			}
			core[eventName] = function(message) {
				if (message) {
					var success = message.success;
					var failed = message.failed;
					var complete = message.complete;

					delete message.success;
					delete message.failed;
					delete message.complete;


					var params = null;
					if (message.hasOwnProperty('params')) {
						if (Object.getOwnPropertyNames(message).length === 1) {
							params = message.params;
						} else {
							params = message;
						}
					} else {
						params = message;
					}

					lr.callEvent({
						eventName: eventName,
						params: params,
						success: success,
						failed: failed,
						complete: complete
					});
				} else {
					lr.callEvent({
						eventName: eventName
					});
				}
			}
		}

		function isNonEmptyArray(obj = []) {
			return (obj && typeof obj === 'object') && Array.isArray(obj) && obj.length > 0
		}

		return {
			config: function(host, message) {
				if (message.debug != null) {
					var params = {
						debug: message.debug
					}
					var request = {
						methodName: 'config',
						params: params
					}
					lr.callNative(request)
				}

				var methodList = message.methodList;
				if (isNonEmptyArray(methodList)) {
					for (var i = 0; i < methodList.length; i++) {
						addMethod(host, methodList[i]);
					}
				}

				var eventList = message.eventList;
				if (isNonEmptyArray(eventList)) {
					for (var i = 0; i < eventList.length; i++) {
						addEvent(host, eventList[i])
					}
				}
			}
		}
	})()
}
