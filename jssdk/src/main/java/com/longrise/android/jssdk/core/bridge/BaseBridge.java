package com.longrise.android.jssdk.core.bridge;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.longrise.android.jssdk.BuildConfig;
import com.longrise.android.jssdk.R;
import com.longrise.android.jssdk.Request;
import com.longrise.android.jssdk.Response;
import com.longrise.android.jssdk.ResponseScript;
import com.longrise.android.jssdk.lifecycle.LifecycleManager;

import java.lang.ref.WeakReference;

/**
 * Created by godliness on 2020-04-09.
 *
 * @author godliness
 */
public abstract class BaseBridge<T> extends BridgeLifecycle<T> implements Handler.Callback, LifecycleManager.OnLifecycleListener {

    private static final String TAG = "BaseBridge";

    private static final int MSG_NOTIFY_NATIVE = 0;
    private static final int MSG_CALL_NATIVE = 1;
    private static final int MSG_CONFIG = 2;

    private WeakReference<WebView> mView;
    private final Handler mHandler;
    private boolean mDebug;

    public BaseBridge() {
        this.mHandler = new Handler(Looper.getMainLooper(), this);
    }

    /**
     * Called after {@link BaseBridge#bindTarget(Object, WebView)}
     */
    protected abstract void onBind();

    protected String bridgeName() {
        return "lrBridge";
    }

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);

        if (BuildConfig.DEBUG) {
            Log.e(TAG, "onDestroy");
        }
    }

    @SuppressLint("AddJavascriptInterface")
    public final void bindTarget(T host, WebView view) {
        super.attachHost(host);
        this.mView = new WeakReference<>(view);
        view.addJavascriptInterface(this, bridgeName());
        onBind();
    }

    protected final WebView getWebView() {
        return mView.get();
    }

    @JavascriptInterface
    public final void onJavaScriptCallFinished(String message) {
        if (!isFinished()) {
            getMessage(MSG_NOTIFY_NATIVE, Response.parseResponse(message)).sendToTarget();
        }
    }

    @JavascriptInterface
    public final void callNativeFromJavaScript(String message) {
        if (!isFinished()) {
            getMessage(MSG_CALL_NATIVE, Request.parseRequest(message)).sendToTarget();
        }
    }

    @JavascriptInterface
    public final void config(String message) {
        if (!isFinished()) {
            getMessage(MSG_CONFIG, Request.parseRequest(message, Config.class)).sendToTarget();
        }
    }

    @Override
    public final boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_NOTIFY_NATIVE:
                final ResponseScript<String> response = parseObjectFromMessage(msg);
                response.onResult();
                if (mDebug) {
                    showMessage(response.serialize());
                }
                return true;

            case MSG_CALL_NATIVE:
                final Request<String> request = parseObjectFromMessage(msg);
                request.dispatchReceiver(getWebView());
                if (mDebug) {
                    showMessage(request.getParams());
                }
                return true;

            case MSG_CONFIG:
                final Request<Config> config = parseObjectFromMessage(msg);
                this.mDebug = config.getParams().debug;
                return true;

            default:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "unknown msg.what: " + msg.what);
                }
                return false;
        }
    }

    static final class Config {
        @Expose
        @SerializedName("debug")
        private boolean debug;
    }

    private void showMessage(String content) {
        if (!isFinished()) {
            new AlertDialog.Builder((Context) getTarget(), R.style.Theme_AppCompat_Light_Dialog_Alert)
                    .setMessage(content)
                    .setCancelable(true).create().show();
        }
    }

    private Message getMessage(int what, Object obj) {
        final Message message = mHandler.obtainMessage(what);
        message.obj = obj;
        return message;
    }

    @SuppressWarnings("unchecked")
    private <Obj> Obj parseObjectFromMessage(Message msg) {
        return (Obj) msg.obj;
    }
}
