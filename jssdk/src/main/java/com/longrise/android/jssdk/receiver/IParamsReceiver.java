package com.longrise.android.jssdk.receiver;

import com.longrise.android.jssdk.receiver.base.ICallbackReceiver;

/**
 * Created by godliness on 2020-04-16.
 *
 * @author godliness
 */
public abstract class IParamsReceiver<P> extends ICallbackReceiver<P> {

    protected abstract void onEvent(P params);

    @Override
    protected final void onReceive(String params, String... args) {
        onEvent(parseParams(params));
    }
}
