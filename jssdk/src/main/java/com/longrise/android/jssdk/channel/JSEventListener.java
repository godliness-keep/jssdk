package com.longrise.android.jssdk.channel;

import android.support.annotation.NonNull;

/**
 * Created by godliness on 2021/7/23.
 *
 * @author godliness
 */
public interface JSEventListener<T extends JSEventChannel<T>> {

    @NonNull
    String[] getEvents();

    void invoke(@NonNull String eventName, @NonNull String params, @NonNull T receiver);
}
