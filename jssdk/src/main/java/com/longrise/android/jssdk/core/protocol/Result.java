package com.longrise.android.jssdk.core.protocol;

import com.longrise.android.jssdk.Response;
import com.longrise.android.jssdk.gson.JsonHelper;
import com.longrise.android.jssdk.gson.ParameterizedTypeImpl;

import java.lang.reflect.Type;

/**
 * Created by godliness on 2020-04-13.
 *
 * @author godliness
 */
public final class Result<T> {

    private int state = Response.RESULT_OK;
    private String desc = "ok";
    private T result;

    private transient String serializedName = "result";

    public static Result<String> parseResult(String json) {
        return parseResult(json, String.class);
    }

    public static <T> Result<T> parseResult(String json, Class<T> clzOfT) {
        return JsonHelper.fromJson(json, ParameterizedTypeImpl.getTypeImpl(Result.class, clzOfT));
    }

    public static <T> Result<T> parseResult(String json, Type typeOfT) {
        return JsonHelper.fromJson(json, ParameterizedTypeImpl.getTypeImpl(Result.class, typeOfT));
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public void setResult(T result, String serializedName) {
        this.result = result;
        this.serializedName = serializedName;
    }

    public T getResult() {
        return result;
    }

    public String serializedName() {
        return serializedName;
    }

    public final String toJson() {
        return JsonHelper.toJson(this);
    }

    @Override
    public String toString() {
        return "Result{" +
                "state=" + state +
                ", desc='" + desc + '\'' +
                ", result=" + result +
                '}';
    }

    public Result() {

    }
}
