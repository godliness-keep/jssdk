package com.longrise.android.jssdk.channel;

import com.longrise.android.jssdk.Response;
import com.longrise.android.jssdk.gson.JsonHelper;
import com.longrise.android.jssdk.receiver.base.BaseReceiver;

import java.lang.reflect.Type;

/**
 * Created by godliness on 2021/7/23.
 *
 * @author godliness
 * <p>
 * ************************************
 * 1、事件所在类的访问权限必须 ≥ 包权限
 * 2、自定义事件方法的参数必须 ≤ 1
 * 3、不允许重复的方法名（同一个宿主内（lifecycle），不允许重复注册相同事件）
 * 4、自定义事件方法不能是抽象的
 * 5、所有事件提供了 return 或 callback 机会（它们各自适用场景不过多解释）
 */
public abstract class JSEventChannel<T extends JSEventChannel<T>> extends BaseReceiver<String> {

    private JSEventListener<T> mEventListener;

    public final <R> void callback(R params) {
        callback(Response.RESULT_OK, "ok", params);
    }

    public final <R> void callback(int state, String desc, R params) {
        Response.create(getId())
                .state(state)
                .desc(desc)
                .result(params)
                .notify(getTarget());
    }

    public final <P> P parseParams(String json, Type type) {
        return JsonHelper.fromJson(json, type);
    }

    public final <P> P parseParams(String json, Class<P> type) {
        return JsonHelper.fromJson(json, type);
    }

    @Override
    protected final String[] findEvents() {
        return mEventListener.getEvents();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected final void onReceive(String params, String... args) {
        mEventListener.invoke(args[0], params, (T) this);
    }

    @SuppressWarnings("unchecked")
    protected JSEventChannel() {
        try {
            mEventListener = (JSEventListener<T>) Class.forName(this.getClass().getName() + "$Events").newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        if (mEventListener == null) {
            throw new NullPointerException("Cannot load to \"" + this.getClass().getName() + "$Events\" class.");
        }
    }
}