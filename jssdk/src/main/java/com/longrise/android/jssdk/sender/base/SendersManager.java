package com.longrise.android.jssdk.sender.base;


import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.View;

import com.longrise.android.jssdk.BuildConfig;
import com.longrise.android.jssdk.ResponseScript;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by godliness on 2020-04-20.
 *
 * @author godliness
 */
public final class SendersManager<T extends ICallback> {

    private static final String TAG = "SendersManager";

    private final ArrayMap<Integer, ArrayMap<Integer, T>> mLifeCycles;
    private final ArrayMap<Integer, Integer> mIds;

    private final SenderLifecycle mCallbackLifecycle;
    private final AtomicInteger mCounter;

    public static SendersManager<? extends ICallback> getManager() {
        return Holder.CALLBACKS_MANAGER;
    }

    public void callbackFromScript(ResponseScript<?> script) {
        final ICallback callback = removeReceiver(script.getCallbackId());
        if (callback != null) {
            callback.onResponse(script);
        }
    }

    int aliveId() {
        return mCounter.incrementAndGet();
    }

    void registerSender(View lifecycle, SenderAgent<?> agent) {
        final int host = lifecycle.hashCode();
        if (!mLifeCycles.containsKey(host)) {
            mCallbackLifecycle.registerSenderLifecycle(lifecycle);
        }
        createEventMap(host, agent.getId(), (T) agent.getCallback());
    }

    void onViewDetachedFromWindow(int host) {
        final ArrayMap<Integer, T> callbacksMap = mLifeCycles.remove(host);
        if (callbacksMap != null) {
            final int size = callbacksMap.size();
            for (int i = 0; i < size; i++) {
                mIds.remove(callbacksMap.keyAt(i));
            }

            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Lifecycle size: " + mLifeCycles.size() + " id size: " + mIds.size());
            }
        }
    }

    private SendersManager() {
        this.mLifeCycles = new ArrayMap<>(3);
        this.mIds = new ArrayMap<>(3);
        this.mCallbackLifecycle = new SenderLifecycle(this);
        this.mCounter = new AtomicInteger(-1);
    }

    private static final class Holder {
        private static final SendersManager<? extends ICallback> CALLBACKS_MANAGER = new SendersManager<>();
    }

    private void createEventMap(int host, int id, T callback) {
        final ArrayMap<Integer, T> receiverMap = createLifecycleCallbacksIfNeed(host);
        if (receiverMap.put(id, callback) != null) {
            throw new IllegalStateException("Cannot have the same \"" + id + "\" in the " + host);
        }
        if (mIds.put(id, host) != null) {
            throw new IllegalStateException("Cannot use the same \"" + id + "\" for different " + host);
        }
    }

    private ArrayMap<Integer, T> createLifecycleCallbacksIfNeed(int host) {
        ArrayMap<Integer, T> receivers = mLifeCycles.get(host);
        if (receivers == null) {
            mLifeCycles.put(host, receivers = new ArrayMap<>(3));
        }
        return receivers;
    }

    private T removeReceiver(int id) {
        final Integer host = mIds.remove(id);
        final ArrayMap<Integer, T> callbacks = mLifeCycles.get(host);
        if (callbacks != null) {
            return callbacks.remove(id);
        }
        return null;
    }
}
