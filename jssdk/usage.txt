com.longrise.android.jssdk.sender.IScriptListener
    public abstract com.longrise.android.jssdk.core.protocol.Result getResult(java.lang.reflect.Type)
    public abstract void deserialize(java.lang.Object)
    public abstract java.lang.Object serialize()
    public abstract void onResult()
com.longrise.android.jssdk.core.JsCallManager
    public void <init>()
    public static void notifyJavaScriptCallNativeFinished(android.webkit.WebView,com.longrise.android.jssdk.Response)
com.longrise.android.jssdk.stream.BaseStream$Chunk
    private static final java.lang.String SCHEMA
com.longrise.android.jssdk.gson.JsonHelper
    public void <init>()
    public static com.google.gson.Gson getGson()
    public static java.lang.Object fromJson(java.io.Reader,java.lang.reflect.Type)
com.longrise.android.jssdk.stream.StreamSender
    public void <init>()
    public static void sendStream(com.longrise.android.jssdk.Response,int,android.webkit.WebView)
    private static final int DEFAULT_CHUNK_LENGTH
com.longrise.android.jssdk.gson.RequestDeserializer
com.longrise.android.jssdk.lifecycle.LifecycleManager
    void unregisterLifecycle()
com.longrise.android.jssdk.gson.GenericHelper
    public void <init>()
    public static java.lang.Class getClassOfT(java.lang.Object,int)
    public static java.lang.Class getClassOfT(java.lang.Class,int)
    public static java.lang.Class getRawType(java.lang.reflect.Type)
com.longrise.android.jssdk.core.bridge.BaseBridge
    private static final java.lang.String TAG
    private static final int MSG_NOTIFY_NATIVE
    private static final int MSG_CALL_NATIVE
    private static final int MSG_CONFIG
com.longrise.android.jssdk.receiver.base.ReceiversManager$Holder
    private void <init>()
com.longrise.android.jssdk.BuildConfig
    public void <init>()
    public static final java.lang.String APPLICATION_ID
    public static final java.lang.String BUILD_TYPE
    public static final java.lang.String FLAVOR
    public static final int VERSION_CODE
    public static final java.lang.String VERSION_NAME
com.longrise.android.jssdk.sender.base.SendersManager
    private static final java.lang.String TAG
com.longrise.android.jssdk.sender.base.SendersManager$Holder
    private void <init>()
com.longrise.android.jssdk.lifecycle.LifecycleManager$Holder
    private void <init>()
com.longrise.android.jssdk.gson.ParameterizedTypeImpl
    public static com.longrise.android.jssdk.gson.ParameterizedTypeImpl getTypeImpl(java.lang.reflect.Type,java.lang.reflect.Type[])
    public void <init>(java.lang.reflect.Type,java.lang.reflect.Type[])
com.longrise.android.jssdk.core.protocol.Result
    public static com.longrise.android.jssdk.core.protocol.Result parseResult(java.lang.String)
    public static com.longrise.android.jssdk.core.protocol.Result parseResult(java.lang.String,java.lang.Class)
    public final java.lang.String toJson()
com.longrise.android.jssdk.receiver.base.ReceiversManager
    private static final java.lang.String TAG
