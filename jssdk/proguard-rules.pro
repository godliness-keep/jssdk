# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
#-allowaccessmodification

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

-printconfiguration configuration.txt
-printusage usage.txt
-printseeds seeds.txt

#-----------------------------------
#          JSSDK
#-----------------------------------
-keep class com.longrise.android.jssdk.Request{
    public static <methods>;
    public void to(android.webkit.WebView);
    public *** getParams();
}

-keep class com.longrise.android.jssdk.Response{
    public static <methods>;
    public static final int RESULT_OK;
}

-keepclassmembers class com.longrise.android.jssdk.sender.base.SenderAgent{
    public <methods>;
}

-keepclassmembers class com.longrise.android.jssdk.sender.INativeListener{*;}
-keepclassmembers class com.longrise.android.jssdk.sender.IMethodListener{*;}
-keepclassmembers class com.longrise.android.jssdk.sender.IEventListener{*;}

-keep class com.longrise.android.jssdk.sender.ResultCallback{
    protected void onReceiveValue(***);
}

-keep class com.longrise.android.jssdk.receiver.IReceiver{
    protected void onEvent();
}

-keep class com.longrise.android.jssdk.receiver.IParamsReceiver{
    protected void onEvent(***);
}

-keep class com.longrise.android.jssdk.receiver.IReturnReceiver{
    protected *** onEvent();
}

-keep class com.longrise.android.jssdk.receiver.IParamsReturnReceiver{
    protected *** onEvent(***);
}

-keepclassmembers class com.longrise.android.jssdk.receiver.base.ICallbackReceiver{
    protected final <methods>;
}

-keepclassmembers class com.longrise.android.jssdk.receiver.base.BaseReceiver{
    public final *** alive();
}

-keep class com.longrise.android.jssdk.receiver.base.EventName

-keepclassmembers class com.longrise.android.jssdk.receiver.base.ReceiverAgent{
    public lifecycle(***);
}

-keep class com.longrise.android.jssdk.core.bridge.BaseBridge{
    protected android.webkit.WebView getWebView();
    public void bindTarget(***, android.webkit.WebView);
    protected java.lang.String bridgeName();
    protected void onBind();
    @android.webkit.JavascriptInterface <methods>;
}

-keep class com.longrise.android.jssdk.core.protocol.Result{
    public get*();
}

-keepclassmembers class com.longrise.android.jssdk.core.bridge.BridgeLifecycle{
    protected void onDestroy();
    protected *** getTarget();
    protected boolean isFinished();
}

-keepclassmembers class com.longrise.android.jssdk.core.protocol.base.AbsDataProtocol{
    public get*();
}

############################################
#               多事件注册
############################################
-keep class com.longrise.android.jssdk.channel.JSEventChannel{
    public final <methods>;
}

-keep class com.longrise.android.jssdk.channel.JSEventListener{
    public <methods>;
}

