package com.longrise.android.jssdk_x5.gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.longrise.android.jssdk_x5.core.protocol.Result;

import java.lang.reflect.Type;

/**
 * Created by godliness on 2021/4/22.
 *
 * @author godliness
 */
final class ResultSerializer implements JsonSerializer<Result<?>> {

    static ResultSerializer create() {
        return new ResultSerializer();
    }

    @Override
    public JsonElement serialize(Result<?> src, Type typeOfSrc, JsonSerializationContext context) {
        final int state = src.getState();
        final String desc = src.getDesc();
        final Object result = src.getResult();

        final JsonObject serialize = new JsonObject();
        serialize.addProperty("state", state);
        serialize.addProperty("desc", desc);
        serialize.add(src.serializedName(), context.serialize(result));
        return serialize;
    }

    private ResultSerializer() {
    }
}
