package com.longrise.android.jssdk_x5.receiver.base;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.ArraySet;
import android.util.Log;

import com.longrise.android.jssdk_x5.BuildConfig;
import com.longrise.android.jssdk_x5.Request;
import com.longrise.android.jssdk_x5.lifecycle.LifecycleManager;
import com.tencent.smtt.sdk.WebView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by godliness on 2020-04-16.
 *
 * @author godliness
 */
public final class ReceiversManager<T extends BaseReceiver<?>> implements LifecycleManager.OnLifecycleListener {

    private static final String TAG = "ReceiversManager";

    private final ArrayMap<Integer, ArrayMap<String, T>> mLifeCycles;
    private final ArrayMap<String, ArraySet<Integer>> mEventNames;

    @SuppressWarnings("unchecked")
    public static <T extends BaseReceiver<?>> ReceiversManager<T> getManager() {
        return (ReceiversManager<T>) Holder.RECEIVER_MANAGER;
    }

    void registerReceiver(Activity lifecycle, ReceiverAgent<?> agent) {
        if (checkActivityState(lifecycle)) {
            throw new IllegalStateException("The host: '" + lifecycle.getClass().getSimpleName() + "' is destroy!");
        }
        createEventMap(lifecycle.hashCode(), agent);
    }

    void registerReceiver(Fragment lifecycle, ReceiverAgent<?> agent) {
        if (lifecycle.isDetached()) {
            throw new IllegalStateException("The host: '" + lifecycle.getClass().getSimpleName() + "' is detached!");
        }
        createEventMap(lifecycle.hashCode(), agent);
    }

    public void dispatchReceiver(Request<String> request, WebView webView) {
        final List<T> receivers = findReceivers(request.getEventName());
        if (receivers != null) {
            for (T receiver : receivers) {
                receiver.notifyReceiver(request, webView);
            }
        }

//        final T receiver = findReceiver(request.getEventName());
//        if (receiver != null) {
//            receiver.notifyReceiver(request, webView);
//        }
    }

    @Override
    public void onActivityFinished(Activity host) {
        removeLifecycleReceiversIfNeed(host.hashCode());

        if (host instanceof FragmentActivity) {
            final FragmentManager fm = ((FragmentActivity) host).getSupportFragmentManager();
            final List<Fragment> fragments = fm.getFragments();
            if (fragments.size() > 0) {
                // 防止系统内部动了 fragments 数组
                // 故每次都要 size()
                for (int i = 0; i < fragments.size(); i++) {
                    removeLifecycleReceiversIfNeed(fragments.get(i).hashCode());
                }
            }
        }
    }

    @Nullable
    private List<T> findReceivers(String eventName) {
        final ArraySet<Integer> hosts = mEventNames.get(eventName);
        if (hosts == null) {
            return null;
        }
        final int size = hosts.size();
        final List<T> receivers = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            final Integer host = hosts.valueAt(i);
            final ArrayMap<String, T> hostReceivers = mLifeCycles.get(host);
            if (hostReceivers != null) {
                receivers.add(hostReceivers.get(eventName));
            }
        }
        return receivers;

//        final Integer host = mEventNames.get(eventName);
//        final ArrayMap<String, T> receivers = mLifeCycles.get(host);
//        if (receivers != null) {
//            return receivers.get(eventName);
//        }
//        return null;
    }

    @SuppressWarnings("unchecked")
    private void createEventMap(Object lifecycle, ReceiverAgent<?> agent) {
        final int host = lifecycle.hashCode();
        final String[] eventNames = agent.getEventName();
        final ArrayMap<String, T> receiversMap = createLifecycleReceiversIfNeed(host);
        for (String eventName : eventNames) {
            final ArraySet<Integer> hosts = createEventHostsIfNeed(eventName);
            if (!hosts.contains(host)) {
                hosts.add(host);
                receiversMap.put(eventName, (T) agent.getReceiver());
            } else {
                throw new IllegalStateException("Cannot use the same \"" + eventName + "\" int the " + lifecycle.getClass().getName());
            }

//            if (receiversMap.put(eventName, (T) agent.getReceiver()) != null) {
//                throw new IllegalStateException("Cannot have the same \"" + eventName + "\" in the " + lifecycle.getClass().getName());
//            }
//            if (mEventNames.put(eventName, host) != null) {
//                throw new IllegalStateException("Cannot use the same \"" + eventName + "\" for different " + lifecycle.getClass().getName());
//            }
        }
    }

    private ArrayMap<String, T> createLifecycleReceiversIfNeed(int host) {
        ArrayMap<String, T> receivers = mLifeCycles.get(host);
        if (receivers == null) {
            mLifeCycles.put(host, receivers = new ArrayMap<>(3));
        }
        return receivers;
    }

    private ArraySet<Integer> createEventHostsIfNeed(String eventName) {
        ArraySet<Integer> hosts = mEventNames.get(eventName);
        if (hosts == null) {
            mEventNames.put(eventName, hosts = new ArraySet<>(3));
            // 允许不同宿主注册相同的事件
//            if (old != null) {
//                throw new IllegalStateException("Cannot use the same \"" + eventName + "\" for different " + lifecycle.getClass().getName());
//            }
        }
        return hosts;
    }

    private void removeLifecycleReceiversIfNeed(int host) {
        final ArrayMap<String, T> receivers = mLifeCycles.remove(host);
        if (receivers != null) {
            for (int i = 0; i < receivers.size(); i++) {
                final String event = receivers.keyAt(i);
                final ArraySet<Integer> hosts = mEventNames.get(event);
                if (hosts != null) {
                    hosts.remove(host);
                    if (hosts.size() <= 0) {
                        mEventNames.remove(event);
                    }
                }
            }
            if (BuildConfig.DEBUG) {
                Log.i(TAG, "Lifecycle size: " + mLifeCycles.size() + " Event size: " + mEventNames.size());
            }
        }

//        final ArrayMap<String, T> receivers = mLifeCycles.remove(host);
//        if (receivers != null) {
//            final int size = receivers.size();
//            for (int i = 0; i < size; i++) {
//                mEventNames.remove(receivers.keyAt(i));
//            }
//            if (BuildConfig.DEBUG) {
//                Log.i(TAG, "Lifecycle size: " + mLifeCycles.size() + " Event size: " + mEventNames.size());
//            }
//        }
    }

    private ReceiversManager() {
        this.mLifeCycles = new ArrayMap<>(5);
        this.mEventNames = new ArrayMap<>(5);
        LifecycleManager.getManager().addLifecycleListener(this);
    }

    private boolean checkActivityState(Activity host) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return host.isDestroyed() && host.isFinishing();
        } else {
            return host.isFinishing();
        }
    }

    private static final class Holder {
        private static final ReceiversManager<?> RECEIVER_MANAGER = new ReceiversManager<>();
    }
}
