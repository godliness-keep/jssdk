package com.longrise.android.jssdk_x5.receiver.base;


import android.support.v4.util.ArrayMap;

import com.longrise.android.jssdk_x5.Request;
import com.longrise.android.jssdk_x5.gson.GenericHelper;
import com.longrise.android.jssdk_x5.gson.JsonHelper;
import com.tencent.smtt.sdk.WebView;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * Created by godliness on 2020-04-15.
 *
 * @author godliness
 */
public abstract class BaseReceiver<P> {

    private int id;
    private int version;
    private WeakReference<WebView> mTarget;

    private static final ArrayMap<String, Type> TYPES = new ArrayMap<>(5);
    private static final ArrayMap<String, String[]> EVENTS = new ArrayMap<>(5);

    public final ReceiverAgent<?> alive() {
        return new ReceiverImpl<>(getEventName(), this);
    }

    protected abstract void onReceive(String params, String... args);

    protected String[] findEvents() {
        return findEventsInternal();
    }

    protected final P parseParams(String json) {
        return JsonHelper.fromJson(json, getGenericType());
    }

    protected final int getId() {
        return id;
    }

    protected final int getVersion() {
        return version;
    }

    protected final WebView getTarget() {
        return mTarget.get();
    }

    final void notifyReceiver(Request<String> request, WebView webView) {
        this.id = request.getCallbackId();
        this.version = request.getVersion();
        this.mTarget = new WeakReference<>(webView);
        onReceive(request.getParams(), request.getEventName());
    }

    private Type getGenericType() {
        final String clzName = getClass().getName();
        final Type type = TYPES.get(clzName);
        if (type == null) {
            TYPES.put(clzName, findGenericType());
        }
        return TYPES.get(clzName);
    }

    private Type findGenericType() {
        return GenericHelper.getTypeOfT(this, 0);
    }

    private String[] getEventName() {
        final String clzName = getClass().getName();
        String[] eventNames = EVENTS.get(clzName);
        if (eventNames == null) {
            EVENTS.put(clzName, eventNames = findEvents());
        }
        return eventNames;
    }

    private String[] findEventsInternal() {
        final Method[] methods = getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(EventName.class)) {
                return new String[]{method.getAnnotation(EventName.class).value()};
            }
        }
        return null;
    }
}
