package com.longrise.android.jssdk_x5.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.longrise.android.jssdk_x5.Request;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by godliness on 2021/4/23.
 *
 * @author godliness
 */
final class RequestDeserializer implements JsonDeserializer<Request<?>> {

    static RequestDeserializer create() {
        return new RequestDeserializer();
    }

    @Override
    public Request<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final Request<Object> request = new Request<>();

        final JsonObject jsonObject = json.getAsJsonObject();
        final JsonElement versionElement = jsonObject.get("version");
        request.setVersion(versionElement != null ? versionElement.getAsInt() : 1);

        final JsonElement idElement = jsonObject.get("id");
        request.setCallbackId(idElement != null ? idElement.getAsInt() : -1);

        final JsonElement eventNameElement = jsonObject.get("eventName");
        request.setEventName(eventNameElement != null ? eventNameElement.getAsString() : "");

        final JsonElement paramsElement = jsonObject.get("params");
        if (paramsElement == null || paramsElement.isJsonNull()) {
            request.setParams(null);
        } else if (paramsElement.isJsonObject()) {
            final Class<?> type = (Class<?>) getTypeOfT(typeOfT);
            if (String.class.isAssignableFrom(type)) {
                request.setParams(paramsElement.getAsJsonObject().toString());
            } else {
                request.setParams(context.deserialize(paramsElement, type));
            }
        } else if (paramsElement.isJsonArray()) {
            final Class<?> type = (Class<?>) getTypeOfT(typeOfT);
            if (String.class.isAssignableFrom(type)) {
                request.setParams(paramsElement.getAsJsonArray().toString());
            } else {
                request.setParams(context.deserialize(paramsElement, type));
            }
        } else if (paramsElement.isJsonPrimitive()) {
            final JsonPrimitive primitive = paramsElement.getAsJsonPrimitive();
            if (primitive.isString()) {
                request.setParams(primitive.getAsString());
            } else if (primitive.isBoolean()) {
                request.setParams(primitive.getAsBoolean());
            } else {
                final Number number = paramsElement.getAsNumber();
                final Class<?> clz = (Class<?>) getTypeOfT(typeOfT);
                if (Integer.class.isAssignableFrom(clz)) {
                    request.setParams(number.intValue());
                } else if (Long.class.isAssignableFrom(clz)) {
                    request.setParams(number.longValue());
                } else if (Double.class.isAssignableFrom(clz)) {
                    request.setParams(number.doubleValue());
                } else if (Float.class.isAssignableFrom(clz)) {
                    request.setParams(number.floatValue());
                } else if (Short.class.isAssignableFrom(clz)) {
                    request.setParams(number.shortValue());
                } else if (Byte.class.isAssignableFrom(clz)) {
                    request.setParams(number.byteValue());
                }
            }
        }
        return request;
    }

    private Type getTypeOfT(Type typeOfT) {
        if (typeOfT instanceof ParameterizedType) {
            return ((ParameterizedType) typeOfT).getActualTypeArguments()[0];
        }
        return null;
    }

    private RequestDeserializer() {
    }
}
