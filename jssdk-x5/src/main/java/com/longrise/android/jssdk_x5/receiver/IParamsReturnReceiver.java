package com.longrise.android.jssdk_x5.receiver;

import com.longrise.android.jssdk_x5.Response;
import com.longrise.android.jssdk_x5.receiver.base.BaseReceiver;

/**
 * Created by godliness on 2020-04-16.
 *
 * @author godliness
 */
public abstract class IParamsReturnReceiver<P, R> extends BaseReceiver<P> {

    protected abstract R onEvent(P params);

    @Override
    protected final void onReceive(String params, String... args) {
        final R r = onEvent(parseParams(params));
        Response.create(getId())
                .result(r)
                .notify(getTarget());
    }
}
