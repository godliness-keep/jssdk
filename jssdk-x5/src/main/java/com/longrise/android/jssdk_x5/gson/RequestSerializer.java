package com.longrise.android.jssdk_x5.gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.longrise.android.jssdk_x5.Request;

import java.lang.reflect.Type;

/**
 * Created by godliness on 2021/4/23.
 *
 * @author godliness
 */
final class RequestSerializer implements JsonSerializer<Request<?>> {

    static RequestSerializer create() {
        return new RequestSerializer();
    }

    @Override
    public JsonElement serialize(Request<?> src, Type typeOfSrc, JsonSerializationContext context) {
        final int version = src.getVersion();
        final String eventName = src.getEventName();
        final int callbackId = src.getCallbackId();

        final JsonObject serialize = new JsonObject();
        serialize.addProperty("version", version);
        serialize.addProperty("eventName", eventName);
        serialize.addProperty("id", callbackId);
        serialize.add("params", context.serialize(src.getParams()));
        return serialize;
    }

    private RequestSerializer() {
    }
}
