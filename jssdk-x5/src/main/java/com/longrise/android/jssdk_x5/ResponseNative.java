package com.longrise.android.jssdk_x5;


import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.longrise.android.jssdk_x5.core.protocol.Result;
import com.longrise.android.jssdk_x5.sender.INativeListener;
import com.longrise.android.jssdk_x5.stream.StreamSender;
import com.tencent.smtt.sdk.WebView;

/**
 * Created by godliness on 2020-04-29.
 *
 * @author godliness
 */
final class ResponseNative<T> extends Response implements INativeListener<T> {

    @Expose(deserialize = false)
    @SerializedName("result")
    private final Result<T> result;

    static <T> INativeListener<T> createInternal(int id) {
        return new ResponseNative<>(id);
    }

    @Override
    public INativeListener<T> state(int state) {
        result.setState(state);
        return this;
    }

    @Override
    public INativeListener<T> desc(String desc) {
        result.setDesc(desc);
        return this;
    }

    @Override
    public INativeListener<T> result(T t) {
        result.setResult(t);
        return this;
    }

    @Override
    public INativeListener<T> result(T t, String serializerName) {
        result.setResult(t, serializerName);
        return this;
    }

    @Override
    public void notify(@NonNull WebView target) {
        StreamSender.sendStream(this, target);
    }

    private ResponseNative(int id) {
        setCallbackId(id);
        this.result = new Result<>();
    }
}
