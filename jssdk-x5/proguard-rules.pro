# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
#-allowaccessmodification

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

-printconfiguration configuration.txt
-printusage usage.txt
-printseeds seeds.txt

#-----------------------------------
#          JSSDK-X5
#-----------------------------------
-keep class com.longrise.android.jssdk_x5.Request{
    public static <methods>;
    public void to(com.tencent.smtt.sdk.WebView);
    public *** getParams();
}

-keep class com.longrise.android.jssdk_x5.Response{
    public static <methods>;
    public static final int RESULT_OK;
}

-keepclassmembers class com.longrise.android.jssdk_x5.sender.base.SenderAgent{
    public <methods>;
}

-keepclassmembers class com.longrise.android.jssdk_x5.sender.INativeListener{*;}
-keepclassmembers class com.longrise.android.jssdk_x5.sender.IMethodListener{*;}
-keepclassmembers class com.longrise.android.jssdk_x5.sender.IEventListener{*;}

-keep class com.longrise.android.jssdk_x5.sender.ResultCallback{
    protected void onReceiveValue(***);
}

-keep class com.longrise.android.jssdk_x5.core.protocol.Result{
    public get*();
}

-keep class com.longrise.android.jssdk_x5.receiver.IReceiver{
    protected void onEvent();
}

-keep class com.longrise.android.jssdk_x5.receiver.IParamsReceiver{
    protected void onEvent(***);
}

-keep class com.longrise.android.jssdk_x5.receiver.IReturnReceiver{
    protected *** onEvent();
}

-keep class com.longrise.android.jssdk_x5.receiver.IParamsReturnReceiver{
    protected *** onEvent(***);
}

-keepclassmembers class com.longrise.android.jssdk_x5.receiver.base.ICallbackReceiver{
    protected final <methods>;
}

-keepclassmembers class com.longrise.android.jssdk_x5.receiver.base.BaseReceiver{
    public final *** alive();
}

-keep class com.longrise.android.jssdk_x5.receiver.base.EventName

-keepclassmembers class com.longrise.android.jssdk_x5.receiver.base.ReceiverAgent{
    public lifecycle(***);
}

-keep class com.longrise.android.jssdk_x5.core.bridge.BaseBridge{
    protected com.tencent.smtt.sdk.WebView getWebView();
    public void bindTarget(***, com.tencent.smtt.sdk.WebView);
    protected java.lang.String bridgeName();
    protected void onBind();
    @android.webkit.JavascriptInterface <methods>;
}

-keepclassmembers class com.longrise.android.jssdk_x5.core.bridge.BridgeLifecycle{
    protected void onDestroy();
    protected *** getTarget();
    protected boolean isFinished();
}

-keepclassmembers class com.longrise.android.jssdk_x5.core.protocol.base.AbsDataProtocol{
    public get*();
}

#-----------------------------------
#          Tencent X5-WebView，作为独立发布的模块时，必须配置该混淆
#-----------------------------------
-dontwarn dalvik.**
-dontwarn com.tencent.smtt.**
-keep class com.tencent.smtt.** {*;}
-keep class com.tencent.tbs.** {*;}

############################################
#               多事件注册
############################################
-keep class com.longrise.android.jssdk_x5.channel.JSEventChannel{
    public final <methods>;
}

-keep class com.longrise.android.jssdk_x5.channel.JSEventListener{
    public <methods>;
}