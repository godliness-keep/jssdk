package com.longrise.android.jssdk.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by godliness on 2021/6/8.
 *
 * @author godliness
 */
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.METHOD})
public @interface JSEvent {
}
